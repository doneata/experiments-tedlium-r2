#!/bin/bash
# Updates the language directories and the search lattices with the new words
# from the UAV commands.

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

. utils/parse_options.sh # accept options

exp_dir=exp_uav
tdnn_affix=1f
nnet3_affix=_cleaned
dir=$exp_dir/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi

# Stage 3
utils/prepare_lang.sh data/local/dict_nosp "<unk>" data/local/lang_nosp data/lang_nosp

# Stage 5
local/format_lms.sh

# Stage 10
utils/mkgraph.sh   data/lang_nosp   $exp_dir/tri1   $exp_dir/tri1/graph_nosp

# Stage 12
utils/mkgraph.sh   data/lang_nosp   $exp_dir/tri2   $exp_dir/tri2/graph_nosp

# Stage 14
utils/prepare_lang.sh data/local/dict      "<unk>" data/local/lang      data/lang

cp -rT data/lang data/lang_rescore
cp data/lang_nosp/G.fst data/lang/
cp data/lang_nosp_rescore/G.carpa data/lang_rescore/

utils/mkgraph.sh   data/lang        $exp_dir/tri2   $exp_dir/tri2/graph

# Stage 15
utils/mkgraph.sh   data/lang        $exp_dir/tri3   $exp_dir/tri3/graph

# Stage 17
utils/mkgraph.sh --self-loop-scale  1.0 \
                   data/lang        $dir            $dir/graph
