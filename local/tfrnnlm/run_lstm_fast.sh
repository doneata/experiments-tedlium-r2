#!/bin/bash

ngram_order=4 # this option when used, the rescoring binary makes an approximation
    # to merge the states of the FST generated from RNNLM. e.g. if ngram-order = 4
    # then any history that shares last 3 words would be merged into one state
stage=1
weight=0.5   # when we do lattice-rescoring, instead of replacing the lm-weights
    # in the lattice with RNNLM weights, we usually do a linear combination of
    # the 2 and the $weight variable indicates the weight for the RNNLM scores

. ./utils/parse_options.sh
. ./cmd.sh
. ./path.sh

set -e

cuda_cmd="queue.pl --gpu 1 --mem 20G"
dir=exp/tfrnnlm_lstm_fast

mkdir -p $dir

steps/tfrnnlm/check_tensorflow_installed.sh

if [ $stage -eq 1 ]; then
  local/tfrnnlm/rnnlm_data_prep.sh $dir
fi

if [ $stage -eq 2 ]; then
# the following script uses TensorFlow. You could use tools/extras/install_tensorflow_py.sh to install it
  # $cuda_cmd $dir/train_rnnlm.log utils/parallel/limit_num_gpus.sh \
    python steps/tfrnnlm/lstm_fast.py --data-path=$dir --save-path=$dir/rnnlm --vocab-path=$dir/wordlist.rnn.final
fi

final_lm=ami_fsh.o3g.kn
LM=$final_lm.pr1-7

if [ $stage -eq 3 ]; then
  for decode_set in "dev" "test"; do
    old_lang_dir=data/tedlium_langs/lang
    decode_dir=exp/chain_cleaned/tdnn1f_sp_bi/decode_${decode_set}_rescore

    # pruned lattice rescoring
    steps/tfrnnlm/lmrescore_rnnlm_lat_pruned.sh \
      --cmd "$decode_cmd --mem 4G" \
      --weight $weight --max-ngram-order $ngram_order \
      $old_lang_dir \
      $dir \
      data/${decode_set}_hires \
      ${decode_dir} \
      ${decode_dir}.tfrnnlm.lattice.${ngram_order}gram
  done
fi

wait
