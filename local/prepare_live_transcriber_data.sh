set -e
set -x

P=live-transcriber-data
mkdir $P

mkdir $P/conf
for f in ivector_extractor online_cmvn splice; do
    cp -L ./exp_uav/nnet3_cleaned/ivectors_test_uav_hires/conf/$f.conf $P/conf/.
done
cp -L ./conf/mfcc_hires.conf $P/conf/mfcc.conf

mkdir $P/ivector_extractor
for f in final.dubm final.ie final.mat global_cmvn.stats online_cmvn.conf splice_opts; do
    cp -L ./exp_uav/nnet3_cleaned/extractor/$f $P/ivector_extractor/.
done

cp -L ./exp_uav_domain_adaptation/n-gram/65536_1/chain_cleaned/tdnn1f_sp_bi/final.mdl $P/.
cp -L ./exp_uav_domain_adaptation/n-gram/65536_1/chain_cleaned/tdnn1f_sp_bi/graph/HCLG.fst $P/.

for f in G.carpa G.fst phones.txt words.txt; do
    cp -L ./data/domain_adaptation/n-gram/65536_1/lang_rescore/$f $P/.
done

cp -L ./exp_uav/chain_cleaned/tdnn1f_sp_bi/graph/phones/word_boundary.int $P/.
