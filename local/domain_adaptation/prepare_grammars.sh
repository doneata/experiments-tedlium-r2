#!/bin/bash

set -e
set -x

n=$1
NR_FOLDS=5

FILE_NAME="%d_%d.%s"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation"
TXT2GRM="/home/doneata/data/uav-commands/grammar/txt2grm.py"

for f in $(seq 1 $NR_FOLDS); do
    txt=$(printf $FILE_NAME $n $f txt)
    grm=$(printf $FILE_NAME $n $f grm)
    far=$(printf $FILE_NAME $n $f far)
    python3 $TXT2GRM \
        $DA_PATH/commands/$txt \
        data/lang/words.txt \
        > $DA_PATH/grammars/$grm
    cd $DA_PATH/grammars
    thraxmakedep $grm
    make 
    farextract $far
    cd -
done
