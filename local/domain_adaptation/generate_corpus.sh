#!/bin/bash

set -e
set -x

n=$1
NR_FOLDS=5

JSGF="/home/doneata/work/drones-state-of-the-art/dataset-prep/voice-commands-2.jsgf"
PROBABILISTIC_GENERATOR="/home/doneata/src/JSGFTools/ProbabilisticGenerator.py"
PYTHON="/home/doneata/src/JSGFTools/venv/bin/python2"
OUT="/home/doneata/data/uav-commands/domain_adaptation/commands/%d_%d.txt"

for f in $(seq 1 $NR_FOLDS); do
    $PYTHON $PROBABILISTIC_GENERATOR $JSGF $n > $(printf $OUT $n $f)
done
