#!/bin/bash

set -e
set -x

n=$1
i=$2

KEY="%d_%d"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation"

function prepare_lang_folders () {
	key=$(printf $KEY $n $i)
	mkdir data/domain_adaptation/grammar/$key	

	for lang in lang lang_nosp lang_nosp_rescore lang_rescore; do
		dst="data/domain_adaptation/grammar/$key/$lang"
		src="data/$lang"

		mkdir $dst
		mkdir $dst/phones

		for f in L.fst L_disambig.fst phones.txt words.txt ; do
			ln -s $(realpath $src/$f) $dst/.
		done

		for f in silence.csl disambig.int; do
			ln -s $(realpath $src/phones/$f) $dst/phones/.
		done

		ln -s ~/data/uav-commands/domain_adaptation/grammars/uav_lm_$key $dst/G.fst
	done

	for lang in lang_nosp_rescore lang_rescore; do
		dst="data/domain_adaptation/grammar/$key/$lang"
		src="data/$lang"
		ln -s $(realpath $src/G.carpa) $dst/.
	done
}

prepare_lang_folders
