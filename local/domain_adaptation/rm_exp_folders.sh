#!/bin/bash

set -e
set -x

if [ $# -eq 0 ]; then
    echo "No arguments provided"
    exit 1
fi 

n=$1
i=$2

KEY="%d_%d"
key=$(printf $KEY $n $i)

rm -rf exp_uav_domain_adaptation/grammar/$key
rm -rf data/domain_adaptation/grammar/$key
