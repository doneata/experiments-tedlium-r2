#!/bin/bash

set -e
# set -x

if [ -f path.sh ]; then . path.sh; fi

lang=data/lang_nosp
f=$lang/words.txt
[ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1

n=$1
NR_FOLDS=5

FILE_NAME_SIZE="%d_%d_%s.%s"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation"

for f in $(seq 1 $NR_FOLDS); do
    # Format language models (as it's done for TED-LIUM, see `local/format_lm.sh`)
    size=big
    lm_=$(printf $FILE_NAME_SIZE $n $f $size lm)
    fst=$(printf $FILE_NAME_SIZE $n $f $size fst)
    arpa2fst \
        --disambig-symbol=#0 \
        --read-symbol-table=$lang/words.txt \
        $DA_PATH/n-grams/$lm_ \
        $DA_PATH/n-grams/$fst
done
