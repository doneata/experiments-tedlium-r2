#!/usr/bin/env bash

set -e
. ./path.sh

n=2048
i=1

. utils/parse_options.sh # accept options

cmd=run.pl  # you might want to set this to queue.pl
use_gpu=true  # use GPU for training

# some options passed into rnnlm-get-egs, relating to sampling.
num_samples=512
sample_group_size=2  # see rnnlm-get-egs
num_egs_threads=10  # number of threads used for sampling, if we're using
                    # sampling.  the actual number of threads that runs at one
                    # time, will be however many is needed to balance the
                    # sampling and the actual training, this is just the maximum
                    # possible number that are allowed to run

rnnlm_max_change=0.5
embedding_max_change=0.5
chunk_length=64
num_epochs=100  # maximum number of epochs to train.  later we
                # may find a stopping criterion.
initial_effective_lrate=0.001
final_effective_lrate=0.0001
embedding_lrate_factor=0.1  # the embedding learning rate is the
                            # nnet learning rate times this factor.

embedding_lrate=0.0001
num_repeats=1
num_iters=175

x=150  # iteration
dir="exp_uav_domain_adaptation/rnn/${n}_${i}"

if $use_gpu; then
    gpu_opt="--use-gpu=yes"
    queue_gpu_opt="--gpu 1"
else
    gpu_opt="--use-gpu=no"
    queue_gpu_opt=""
fi

this_learning_rate=0.0001
src_rnnlm="nnet3-copy --learning-rate=$this_learning_rate $dir/$x.raw -|"

if $sparse_features; then
    sparse_opt="--read-sparse-word-features=$dir/word_feats.txt"
    embedding_type=feat
else
    sparse_opt=''
    embedding_type=word
fi

if [ -f $dir/feat_embedding.$x.mat ]; then
    sparse_features=true
    embedding_type=feat
    if [ -f $dir/word_embedding.$x.mat ]; then
        echo "$0: error: $dir/feat_embedding.0.mat and $dir/word_embedding.0.mat both exist."
        exit 1;
    fi
    ! [ -f $dir/word_feats.txt ] && echo "$0: expected $dir/word_feats.txt to exist" && exit 1;
else
    sparse_features=false
    embedding_type=word
    ! [ -f $dir/word_embedding.0.mat ] && \
        echo "$0: expected $dir/word_embedding.0.mat to exist" && exit 1
fi

p=$dir/1.txt
repeated_data=$(for n in $(seq $num_repeats); do echo -n $p; done)

num_splits_processed=0
vocab_size=$(tail -n 1 exp_uav/rnnlm_lstm_e/config/words.txt | awk '{print $NF + 1}')
train_egs_args="--vocab-size=$vocab_size $(cat $dir/special_symbol_opts.txt)"

if [ -f $dir/sampling.lm ]; then
  # we are doing sampling.
  train_egs_args="$train_egs_args --num-samples=$num_samples --sample-group-size=$sample_group_size --num-threads=$num_egs_threads $dir/sampling.lm"
fi

while [ $x -lt $num_iters ]; do
    dest_number=$[x+1]
    $cmd $queue_gpu_opt $dir/log/train.$x.log \
       rnnlm-train \
         --rnnlm.max-param-change=$rnnlm_max_change \
         --embedding.max-param-change=$embedding_max_change \
         --embedding.learning-rate=$embedding_lrate \
         $sparse_opt $gpu_opt \
         --read-rnnlm="$src_rnnlm" --write-rnnlm=$dir/$dest_number.raw \
         --read-embedding=$dir/${embedding_type}_embedding.$x.mat \
         --write-embedding=$dir/${embedding_type}_embedding.$dest_number.mat \
         "ark,bg:cat $repeated_data | rnnlm-get-egs --srand=$num_splits_processed $train_egs_args - ark:- |" || touch $dir/.train_error &
    wait
    x=$[x+1]
done

# Create links
cd $dir
ln -s $num_iters.raw final.raw
ln -s feat_embedding.$num_iters.mat feat_embedding.final.mat
cd -
