#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

lang=grammar
n=2048
i=1

nj=8
decode_nj=8  # note: should not be greater than the number of speakers
             # We decode with 4 threads, so this will be too many jobs if
             # you're using run.pl.
stage=0
dset=test_uav

. utils/parse_options.sh # accept options

test_dir=data/${dset}

key=$(printf "%d_%d" $n $i)
exp_dir=exp_uav_domain_adaptation/$lang/$key
lang_path=data/domain_adaptation/$lang/$key

if [ $stage -eq 1 ]; then
    utils/mkgraph.sh \
        $lang_path/lang_nosp \
        $exp_dir/tri1 \
        $exp_dir/tri1/graph_nosp

    steps/decode.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri1/graph_nosp \
        $test_dir \
        $exp_dir/tri1/decode_nosp_${dset}

    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri1/graph_nosp \
        $exp_dir/tri1/decode_nosp_${dset}
fi

if [ $stage -eq 2 ]; then
    utils/mkgraph.sh \
        $lang_path/lang_nosp \
        $exp_dir/tri2 \
        $exp_dir/tri2/graph_nosp

    # Decode
    steps/decode.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri2/graph_nosp \
        $test_dir \
        $exp_dir/tri2/decode_nosp_${dset}

    # Evaluate
    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri2/graph_nosp \
        $exp_dir/tri2/decode_nosp_${dset}
fi

if [ $stage -eq 3 ]; then
    utils/mkgraph.sh \
        $lang_path/lang \
        $exp_dir/tri2 \
        $exp_dir/tri2/graph

    steps/decode.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri2/graph \
        $test_dir \
        $exp_dir/tri2/decode_${dset}

    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri2/graph \
        $exp_dir/tri2/decode_${dset}
fi

if [ $stage -eq 4 ]; then
    utils/mkgraph.sh \
        $lang_path/lang \
        $exp_dir/tri3 \
        $exp_dir/tri3/graph

    steps/decode_fmllr.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri3/graph \
        $test_dir \
        $exp_dir/tri3/decode_${dset}

    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri3/graph \
        $exp_dir/tri3/decode_${dset}
fi

if [ $stage -eq 5 ]; then
    nnet3_affix=_cleaned
    tdnn_affix=1f
    d=${exp_dir}/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi
    online_ivector_dir=exp_uav/nnet3_cleaned/ivectors_${dset}_hires

    utils/mkgraph.sh \
        --self-loop-scale 1.0 \
        $lang_path/lang \
        $d \
        $d/graph

    # Decode
    steps/nnet3/decode.sh \
        --num-threads 4 \
        --nj $decode_nj \
        --cmd "$decode_cmd" \
        --acwt 1.0 \
        --post-decode-acwt 10.0 \
        --online-ivector-dir ${online_ivector_dir} \
        --skip_scoring true \
        $d/graph \
        data/${dset}_hires \
        $d/decode_${dset}

    # Evaluate
    local/score_basic.sh \
        data/${dset}_hires \
        $d/graph \
        $d/decode_${dset}

    # Rescore
    steps/lmrescore_const_arpa.sh \
        --cmd "$decode_cmd" \
        --skip_scoring true \
        $lang_path/lang \
        $lang_path/lang_rescore \
        data/${dset}_hires \
        $d/decode_${dset} \
        $d/decode_${dset}_rescore

    # Evaluate
    local/score_basic.sh \
        data/${dset}_hires \
        $lang_path/lang_rescore \
        $d/decode_${dset}_rescore
fi
