#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

lang=n-gram
n=2048
i=1

nj=8
decode_nj=8  # note: should not be greater than the number of speakers
             # We decode with 4 threads, so this will be too many jobs if
             # you're using run.pl.

ngram_order=4
dset=test_uav

. utils/parse_options.sh # accept options

test_dir=data/${dset}

key=$(printf "%d_%d" $n $i)
exp_dir=exp_uav_domain_adaptation/$lang/$key
lang_path=data/domain_adaptation/$lang/$key

nnet3_affix=_cleaned
tdnn_affix=1f
d=${exp_dir}/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi
online_ivector_dir=exp_uav/nnet3_cleaned/ivectors_test_uav_hires

# Rescore
rnnlm/lmrescore_pruned.sh \
    --cmd "$decode_cmd --mem 16G -l hostname=b*" \
    --weight 0.5 --max-ngram-order $ngram_order \
    $lang_path/lang \
    exp_uav_domain_adaptation/rnn/$key \
    data/${dset}_hires \
    $d/decode_${dset} \
    $d/decode_${dset}_rescore_rnn

# Evaluate
local/score_basic.sh \
    data/${dset}_hires \
    $lang_path/lang \
    $d/decode_${dset}_rescore_rnn
