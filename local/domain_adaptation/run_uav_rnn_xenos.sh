#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

lang=n-gram
n=2048
i=1
modality=text
test_set=text
assoc_type=annotated
only_eval="false"

nj=8
decode_nj=8  # note: should not be greater than the number of speakers
             # We decode with 4 threads, so this will be too many jobs if
             # you're using run.pl.

ngram_order=4
dset=test_uav

. utils/parse_options.sh # accept options

test_dir=data/${dset}

key=$(printf "%d_%d" $n $i)
xenos_key=$(printf "%s-%d-%d" $modality $n $i)

exp_dir=exp_uav_domain_adaptation/$lang/$key
lang_path=data/domain_adaptation/$lang/$key

nnet3_affix=_cleaned
tdnn_affix=1f
d=${exp_dir}/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi
online_ivector_dir=exp_uav/nnet3_cleaned/ivectors_${dset}_hires

if [ ${test_set} == "text" ]; then
    test_dir=data/${dset}_hires
elif [ ${test_set} == "img" ]; then
    test_dir=data/${dset}_img
else
    echo "Unkown test set ${test_set}"
    exit 1
fi

suffix="rescore_rnn.xenos-${modality}"

if [ ${modality} == "text" ]; then
    suffix=${suffix}
elif [ ${modality} == "img" ]; then
    # assoc-type is relevant only when the modality includes images
    suffix=${suffix}-${assoc_type}
else
    echo "Unkown modalty ${modality}"
    exit 1
fi

if [ ${only_eval} != "true" ]; then
    # Rescore
    rnnlm/lmrescore_nbest_xenos.sh \
        --cmd "$decode_cmd --mem 4G" --N 50 \
        --skip-scoring true \
        --assoc-type $assoc_type \
        0.8 \
        $lang_path/lang \
        $xenos_key \
        data/${dset}_hires \
        $d/decode_${dset} \
        $d/decode_${dset}_${suffix}
fi

# Evaluate
local/score_basic.sh \
    $test_dir \
    $lang_path/lang \
    $d/decode_${dset}_${suffix}
