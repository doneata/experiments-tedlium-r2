#!/bin/bash

set -e
set -x

n=$1
i=$2

KEY="%d_%d"
LANG_DIR="n-gram_large"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation"

function prepare_lang_folders () {
    key=$(printf $KEY $n $i)
    mkdir data/domain_adaptation/$LANG_DIR/$key

    for lang in lang lang_nosp; do
        dst="data/domain_adaptation/$LANG_DIR/$key/$lang"
        src="data/$lang"

        mkdir $dst
        mkdir $dst/phones

        for f in L.fst L_disambig.fst phones.txt words.txt ; do
            ln -s $(realpath $src/$f) $dst/.
        done

        for f in silence.csl disambig.int; do
            ln -s $(realpath $src/phones/$f) $dst/phones/.
        done

        ln -s ~/data/uav-commands/domain_adaptation/n-grams/${key}_big.fst $dst/G.fst
    done
}

prepare_lang_folders
