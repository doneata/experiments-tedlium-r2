#!/bin/bash

set -e
# set -x

if [ -f path.sh ]; then . path.sh; fi

lang=data/lang_nosp
f=$lang/words.txt
[ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1

n=$1
NR_FOLDS=5

FILE_NAME="%d_%d.%s"
FILE_NAME_SIZE="%d_%d_%s.%s"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation"

for f in $(seq 1 $NR_FOLDS); do
    # Learn n-gram on the UAV dataset
    txt=$(printf $FILE_NAME $n $f txt)
    lm=$(printf $FILE_NAME $n $f lm)
    ngram-count \
        -text $DA_PATH/commands/$txt \
        -order 4 \
        -lm $DA_PATH/n-grams/$lm

    # Interpolate n-gram with the TED-LIUM "small" and "big" n-grams
    size=small
    lm_=$(printf $FILE_NAME_SIZE $n $f $size lm)
    ngram \
        -lambda 0.1 \
        -lm $DA_PATH/n-grams/4gram_${size}.arpa \
        -mix-lm $DA_PATH/n-grams/$lm \
        -order 4 \
        -write-lm $DA_PATH/n-grams/$lm_

    size=big
    lm_=$(printf $FILE_NAME_SIZE $n $f $size lm)
    ngram \
        -lambda 0.01 \
        -lm $DA_PATH/n-grams/4gram_${size}.arpa \
        -mix-lm $DA_PATH/n-grams/$lm \
        -order 4 \
        -write-lm $DA_PATH/n-grams/$lm_

    # Format language models (as it's done for TED-LIUM, see `local/format_lm.sh`)
    size=small
    lm_=$(printf $FILE_NAME_SIZE $n $f $size lm)
    fst=$(printf $FILE_NAME_SIZE $n $f $size fst)
    arpa2fst \
        --disambig-symbol=#0 \
        --read-symbol-table=$lang/words.txt \
        $DA_PATH/n-grams/$lm_ \
        $DA_PATH/n-grams/$fst

    size=big
    lm_=$(printf $FILE_NAME_SIZE $n $f $size lm)
    carpa=$(printf $FILE_NAME_SIZE $n $f $size carpa)

    unk=`cat $lang/oov.int`
    bos=`grep -w "<s>" $lang/words.txt | awk '{print $2}'`
    eos=`grep "</s>"   $lang/words.txt | awk '{print $2}'`

    arpa-to-const-arpa \
        --bos-symbol=$bos \
        --eos-symbol=$eos \
        --unk-symbol=$unk \
        "cat $DA_PATH/n-grams/$lm_ | utils/map_arpa_lm.pl $lang/words.txt|" \
        $DA_PATH/n-grams/$carpa
done
