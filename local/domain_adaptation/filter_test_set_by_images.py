import os
import pdb


def load_commands(path):
    def process(line):
        line = line.strip()
        id_, *_ = line.split(' ')
        user_id, _, command_id = id_.split('_')
        return command_id, line
    with open(path, "r") as f:
        return [process(line) for line in f.readlines()]


def load_images(dir_):
    images = os.listdir(dir_)
    images = [im.split('.')[0] for im in images if im.endswith('.jpg')]
    return set(images)


def main():
    TEXT_PATH = "data/test_uav_hires/text"
    IMG_DIR = os.path.expanduser("~/data/uav-commands/images")
    images = load_images(IMG_DIR)
    commands = load_commands(TEXT_PATH)
    with open('data/test_uav_img/text', 'w') as f:
        for i, command in commands:
            if i not in images:
                continue
            f.write(command)
            f.write('\n')


if __name__ == "__main__":
    main()
