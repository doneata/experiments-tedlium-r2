#!/bin/bash

set -e
set -x

lang=$1
n=$2
i=$3

if [ $lang != "grammar" ] && [ $lang != "n-gram" ] && [ $lang != "n-gram_large" ]; then
    echo "Unknown language model $lang"
    exit 1
fi

KEY="%d_%d"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation"

function mktri () {
    j=$1
    path=$2

    src="exp/tri$j"
    dst="$path/tri$j"

    mkdir $dst

    for f in final.mdl final.occs phones.txt tree; do
        ln -s $(realpath $src/$f) $dst/$f
    done

	case $j in
	2)
		for f in final.mat full.mat; do
			ln -s $(realpath $src/$f) $dst/$f
		done
		;;
	3)
		for f in cmvn_opts final.alimdl final.mat full.mat; do
			ln -s $(realpath $src/$f) $dst/$f
		done
		;;
	esac
}

function mkchain () {
    path=$1

    src="exp/chain_cleaned/tdnn1f_sp_bi"
    dst="$path/chain_cleaned/tdnn1f_sp_bi"

    mkdir -p $dst

    for f in cmvn_opts den.fst final.mdl frame_subsampling_factor lda.mat normalization.fst phone_lm.fst tree; do
        ln -s $(realpath $src/$f) $dst/.
    done
}

function prepare_exp_folders () {
    path=exp_uav_domain_adaptation/$lang/$(printf $KEY $n $i)

    mkdir $path

    mktri 1 $path
    mktri 2 $path
    mktri 3 $path

    mkchain $path
}

prepare_exp_folders
