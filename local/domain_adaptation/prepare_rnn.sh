#!/bin/bash

set -e
set -x

n=$1
NR_FOLDS=5

NAME="%d_%d"
DA_PATH="/home/doneata/data/uav-commands/domain_adaptation/rnn"

SRC_PATH="exp_uav/rnnlm_lstm_e"
DST_PATH="exp_uav_domain_adaptation/rnn"

function prepare_data () {
    f=$1
    name=$(printf $NAME $n $f)
    dst_path=$DST_PATH/$name
    mkdir -p $dst_path
    mkdir -p $dst_path/config
    echo "train 1 1.0" > $dst_path/config/data_weights.txt
    python3 rnnlm/prepare_split_data.py \
        --vocab-file $SRC_PATH/config/words.txt \
        --data-weights $dst_path/config/data_weights.txt \
        --num-splits 1 \
        --unk-word '<unk>' \
        $DA_PATH/$name \
        $dst_path
    for f in "150.raw" "feat_embedding.150.mat" "word_feats.txt" "sampling.lm" "special_symbol_opts.txt"; do
        ln -s $(realpath $SRC_PATH/$f) $dst_path/.
    done
    for f in "oov.txt" "unigram_probs.txt" "words.txt"; do
        ln -s $(realpath $SRC_PATH/config/$f) $dst_path/config/.
    done
}

for f in $(seq 1 $NR_FOLDS); do
    prepare_data $f
done
