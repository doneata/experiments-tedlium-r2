#!/bin/bash

# set -e
# set -x

function format_sclite {
    awk '
    {
        for (i=2; i<=NF; i++) {
            printf("%s ", $i);
        }
        split($1, ids, "_");
        printf("(%s/%s)\n", ids[1], $1);
    }
    ' $1
}

tmp1=$(mktemp)
tmp2=$(mktemp)

sc="/home/doneata/src/kaldi/tools/sctk/bin/sclite"

cat exp_uav_grammar/chain_cleaned/tdnn1f_sp_bi/decode_test_uav_rescore/scoring/10.tra \
    | ./utils/int2sym.pl -f 2- data/lang/words.txt \
    | format_sclite \
    > $tmp1

cat data/test_uav/text \
    | format_sclite \
    > $tmp2

$sc -h $tmp1 trn -r $tmp2 trn -i spu_id

rm -f $tmp1 $tmp2
