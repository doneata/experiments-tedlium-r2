#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

nj=4
decode_nj=4  # note: should not be greater than the number of speakers
             # We decode with 4 threads, so this will be too many jobs if
             # you're using run.pl.
dset=""
data=/home/doneata/work/xts/data/grid/kaldi

. utils/parse_options.sh # accept options

if [ -z $dset ]; then
    echo "Missing dset"
    exit 1
fi

num_speakers=$(wc -l ${data}/${dset}/spk2utt | cut -f1 -d" ")

if [ $num_speakers -lt 4 ]; then
    decode_nj=$num_speakers
    nj=$num_speakers
fi

exp_dir=exp_grid
test_dir=${data}/${dset}

steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $test_dir
steps/compute_cmvn_stats.sh $test_dir

nnet3_affix=_cleaned
tdnn_affix=1f
d=${exp_dir}/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi

# Extract iVectors
utils/copy_data_dir.sh ${data}/$dset ${data}/${dset}_hires

steps/make_mfcc.sh \
    --nj $nj \
    --mfcc-config conf/mfcc_hires.conf \
    --cmd "$train_cmd" \
    ${data}/${dset}_hires

steps/compute_cmvn_stats.sh \
    ${data}/${dset}_hires

utils/fix_data_dir.sh \
    ${data}/${dset}_hires

steps/online/nnet2/extract_ivectors_online.sh \
    --cmd "$train_cmd" \
    --nj "$nj" \
    ${data}/${dset}_hires \
    exp/nnet3${nnet3_affix}/extractor \
    ${exp_dir}/nnet3${nnet3_affix}/ivectors_${dset}_hires

if [ ! -d $d/graph-grammar ]; then
    utils/mkgraph.sh --self-loop-scale 1.0 data/lang-grid-grammar $d $d/graph-grammar
fi

# Decode
steps/nnet3/decode.sh \
    --num-threads 4 \
    --nj $decode_nj \
    --cmd "$decode_cmd" \
    --acwt 1.0 \
    --post-decode-acwt 10.0 \
    --online-ivector-dir ${exp_dir}/nnet3${nnet3_affix}/ivectors_${dset}_hires \
    --skip_scoring true \
    $d/graph-grammar \
    ${data}/${dset}_hires \
    $d/decode-grammar_${dset}

# Evaluate
local/score_basic.sh \
    ${data}/${dset}_hires \
    $d/graph-grammar \
    $d/decode-grammar_${dset}
