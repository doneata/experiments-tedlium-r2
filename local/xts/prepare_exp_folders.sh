#!/bin/bash

set -e
set -x

dset=$1

function mkchain () {
    path=$1

    src="exp/chain_cleaned/tdnn1f_sp_bi"
    dst="$path/chain_cleaned/tdnn1f_sp_bi"

    mkdir -p $dst

    for f in cmvn_opts den.fst final.mdl frame_subsampling_factor lda.mat normalization.fst phone_lm.fst tree; do
        ln -s $(realpath $src/$f) $dst/.
    done
}

function prepare_exp_folders () {
    path=exp_${dset}
    mkdir $path
    mkchain $path
}

prepare_exp_folders
