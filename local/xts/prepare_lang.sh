#!/bin/bash

set -e
set -x

stage=$1
dset=$2

if [ $stage -eq 1 ]; then
    cp -r exp_{uav,$dset}/chain_cleaned/tdnn1f_sp_bi/graph
fi

if [ $stage -eq 2 ]; then
    dir=data/lang-$dset   
    srcdir=data/local/dict_nosp
    tmpdir=data/local/lang_nosp
    sil_prob=0.5
    silphone=`cat $srcdir/optional_silence.txt` || exit 1;
    cp -r data/lang $dir
    utils/lang/make_lexicon_fst.py \
           --sil-prob=$sil_prob \
           --sil-phone=$silphone \
           $tmpdir/lexiconp_${dset}.txt | \
       fstcompile \
           --isymbols=$dir/phones.txt \
           --osymbols=$dir/words.txt \
           --keep_isymbols=false \
           --keep_osymbols=false | \
       fstarcsort --sort_type=olabel > $dir/L.fst || exit 1;
fi
