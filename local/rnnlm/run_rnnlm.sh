#!/bin/bash

ngram_order=4
rnndir=exp/rnnlm_lstm_e
decode_set=dev
rescore=lattice

. ./utils/parse_options.sh
. ./cmd.sh
. ./path.sh

set -e

# ./local/rnnlm/tuning/run_lstm_e.sh

dir=exp/chain_cleaned/tdnn1f_sp_bi
decode_dir=${dir}/decode_${decode_set}_rescore
# Using a back-up path, instead of the standard data/lang, because I have since
# updated the language models to account for the missing words in the UAV
# dictionary (for more information see my journal entry from 2017-12-05).
old_lang_dir=data/tedlium_langs/lang

if [ $rescore == "lattice" ]; then
    # Lattice rescoring
    rnnlm/lmrescore_pruned.sh \
        --cmd "$decode_cmd --mem 16G -l hostname=b*" \
        --weight 0.5 --max-ngram-order $ngram_order \
        $old_lang_dir \
        $rnndir \
        data/${decode_set}_hires \
        ${decode_dir} \
        ${decode_dir}.kaldirnnlm.${rescore}.${ngram_order}gram
elif [ $rescore == "nbest" ]; then
    # N-best rescoring
    rnnlm/lmrescore_nbest.sh \
        --cmd "$decode_cmd --mem 4G" --N 50 \
        0.8 \
        $old_lang_dir \
        $rnndir \
        data/${decode_set}_hires \
        ${decode_dir} \
        ${decode_dir}.kaldirnnlm.${rescore}.${ngram_order}gram
fi
