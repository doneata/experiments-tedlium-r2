#!/bin/bash

method=$1

BASEPATH=exp/chain_cleaned/tdnn1f_sp_bi

function score_dev {
    local/score_basic.sh data/dev data/lang $BASEPATH/$1
}

function score_test {
    local/score_basic.sh data/dev data/lang $BASEPATH/$1
}

case $method in
    "baseline")
        score_dev  decode_dev
        score_test decode_test
        ;;
    "rescoring")
        ;;
    *)
        echo "Unknown method $method"
        ;;
esac
