# set -e
# set -x

size=$1
fold=$2

ref="data/test_uav/text"
out="exp_uav_domain_adaptation/significance-tests"

outfile="exp_uav_domain_adaptation/significance-tests/%d_%d_%s"

base="exp_uav_domain_adaptation/n-gram/%d_%d/chain_cleaned/tdnn1f_sp_bi/decode_test_uav_noise_snr_10_rescore_rnn.xenos-%s/scoring/10.tra"
hyp1=$(printf $base $size $fold text)
hyp2=$(printf $base $size $fold img-annotated)
hyp3=$(printf $base $size $fold img-generated)

function format_sclite {
    awk '
    {
        for (i=2; i<=NF; i++) {
            printf("%s ", $i);
        }
        split($1, ids, "_");
        printf("(%s/%s)\n", ids[1], $1);
    }
    ' $1
}

sclite="/home/doneata/src/kaldi/tools/sctk/bin/sclite"
sc_stats="/home/doneata/src/kaldi/tools/sctk/bin/sc_stats"

cat $ref | format_sclite > $out/ref.txt

out1=$(printf $outfile $size $fold txt)
cat $hyp1 | ./utils/int2sym.pl -f 2- data/lang/words.txt | format_sclite > $out1
$sclite -h $out1 trn -r $out/ref.txt trn -i spu_id -o sgml

out2=$(printf $outfile $size $fold img-a)
cat $hyp2 | ./utils/int2sym.pl -f 2- data/lang/words.txt | format_sclite > $out2
$sclite -h $out2 trn -r $out/ref.txt trn -i spu_id -o sgml

out3=$(printf $outfile $size $fold img-g)
cat $hyp3 | ./utils/int2sym.pl -f 2- data/lang/words.txt | format_sclite > $out3
$sclite -h $out3 trn -r $out/ref.txt trn -i spu_id -o sgml

cat $out1.sgml $out2.sgml $out3.sgml | $sc_stats -p -t mapsswe -O $out -n ${size}_${fold}
