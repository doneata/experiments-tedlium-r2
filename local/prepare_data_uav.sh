# Useful paths
I=/home/doneata/data/uav-commands  # Input
O=data/test_uav  # Output

# wav.scp
find $I/16khz -name '*wav' | sort > /tmp/uav-commands-paths.txt
find $I/16khz -name '*wav' -printf "%f\n" | sort > /tmp/uav-commands-files.txt

paste -d" " \
    <(cut -f1 -d"." /tmp/uav-commands-files.txt) \
    <(cat /tmp/uav-commands-paths.txt) \
    > $O/wav.scp

# utt2spk
paste -d" " \
    <(cut -f1 -d" " $O/wav.scp) \
    <(cut -f1 -d"_" $O/wav.scp) \
    > $O/utt2spk

# spk2utt
./utils/utt2spk_to_spk2utt.pl $O/utt2spk > $O/spk2utt

# text
cat $I/commands/*txt | sort > $O/text
