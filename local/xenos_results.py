#!/usr/bin/python3

import argparse
import os
import pdb

import numpy as np

from scipy.stats import sem

from uav_results import (
    ROOT,
    get_path_to_scores,
    get_wers,
    get_wers_ted,
)


DECODING = {
    'n-gram': {
        'str': 'n-gram small',
        'name': 'n-gram',
    },
}


RESCORING = {
    'rnn-xenos': {
        'str': 'rnn-xenos',
        'suffix': '_rescore_rnn.xenos-text',
    },
    'rnn-xenos-img-a': {
        'str': 'rnn-xenos img annotated',
        'suffix': '_rescore_rnn.xenos-img-annotated',
    },
    'rnn-xenos-img-g': {
        'str': 'rnn-xenos img generated',
        'suffix': '_rescore_rnn.xenos-img-generated',
    },
}


def get_wer(decoding, rescoring, nr_sents, fold, dset='test_uav', lmwt=None):
    """Pick the value for the LMWT hyper-parameter based on TED-LIUM DEV."""
    path = os.path.join(
        ROOT,
        'exp_uav_domain_adaptation/{:s}/{:d}_{:d}/chain_cleaned/tdnn1f_sp_bi/decode_{:s}{:s}')
    wers_uav = get_wers(path.format(DECODING[decoding]['name'], nr_sents, fold, dset, RESCORING[rescoring]['suffix']))
    wer = wers_uav[lmwt]
    return wer


ROW_FORMAT_SHORT = '{:5.2f} ± {:.1f} |'
ROW_FORMAT_SHORT_TEX = '\\res{{{:.2f}}}{{{:.1f}}} &'
ROW_FORMAT_LONG = '| {:5d} | {:15s} | {:15s} | {:5.2f} ± {:.1f} |'


def main():
    parser = argparse.ArgumentParser(description='Script to fetch WER results for the UAV set.')
    # parser.add_argument('-d', '--decoding', choices=('fsg', 'n-gram', 'n-gram_large'), help='which decoding method to use')
    parser.add_argument('-r', '--rescoring', choices=RESCORING, required=True, help='which rescoring method to use')
    # parser.add_argument('--lmwt', type=int, choices=list(range(7, 17)), default=None, help='language model weight')
    parser.add_argument('--dset', default='test_uav', help='specify data set, useful for noise experiments')
    parser.add_argument('-n', '--nr-sents', type=int, help='size of train corpus')
    parser.add_argument('--fmt', choices=('short', 'short-tex', 'long'), default='short', help='how to format the results')
    args = parser.parse_args()

    D = 'n-gram'
    R = args.rescoring
    LMWT = 10

    wers = [get_wer(D, R, args.nr_sents, f + 1, dset=args.dset, lmwt=LMWT) for f in range(5)]

    N_SEM = 2

    if args.fmt == 'short':
        print(ROW_FORMAT_SHORT.format(np.mean(wers), N_SEM * sem(wers)), end=' ')
    if args.fmt == 'short-tex':
        print(ROW_FORMAT_SHORT_TEX.format(np.mean(wers), N_SEM * sem(wers)), end=' ')
    elif args.fmt == 'long':
        print(ROW_FORMAT_LONG.format(
            args.nr_sents,
            DECODING[D]['str'],
            RESCORING[R]['str'],
            np.mean(wers),
            N_SEM * sem(wers),
        ))


if __name__ == '__main__':
    main()
