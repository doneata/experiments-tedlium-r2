import argparse
import pdb

from itertools import product

import altair as alt

from numpy import mean

from scipy.stats import sem

import pandas as pd

from domain_adaptation_results import get_wer


SETTINGS = {
    # 'fsg': ('fsg', 'none'),    
    'n-gram': ('n-gram', 'none'),    
    'n-gram large': ('n-gram_large', 'none'),    
    'n-gram → n-gram large': ('n-gram', 'n-gram'),    
    'n-gram → rnn': ('n-gram', 'rnn'),    
}

LMWTs = range(7, 18)


def stats(xs):
    xs = list(xs)
    return mean(xs), sem(xs)


def main():
    parser = argparse.ArgumentParser(description='Script to fetch WER results for the UAV set.')
    parser.add_argument('-n', '--nr-sents', type=int, help='size of train corpus')
    args = parser.parse_args()

    wers = {
        (s, t): stats(get_wer(*SETTINGS[s], args.nr_sents, f + 1, t) for f in range(5))
        for s, t in product(SETTINGS, LMWTs)
    }

    wers = pd.DataFrame(wers).T.reset_index()
    wers = wers.rename(columns={
        'level_0': 'Method',
        'level_1': 'LMWT',
        0: 'WER',
        1: 'SEM',
    })
    wers['WER-min'] = wers['WER'] - 2 * wers['SEM']
    wers['WER-max'] = wers['WER'] + 2 * wers['SEM']

    line = alt.Chart(wers).mark_line().encode(
        x='LMWT',
        y='WER',
        color='Method',
    ).properties(
        title='N = {:d}'.format(args.nr_sents),
    )
    confidence_interval = alt.Chart(wers).mark_area(opacity=0.3).encode(
        x='LMWT',
        y=alt.Y('WER-min', axis=alt.Axis(title='WER')),
        y2='WER-max',
        color='Method',
    )
    chart = confidence_interval + line

    path = 'exp_uav_domain_adaptation/plots/lmwt_n-{:d}.png'.format(args.nr_sents)
    chart.save(path, webdriver='firefox')


if __name__ == '__main__':
    main()
