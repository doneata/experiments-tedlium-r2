import argparse
import pdb

from itertools import product

import altair as alt

from numpy import mean

from scipy.stats import sem

import pandas as pd

from domain_adaptation_results import get_wer as get_wer_d
from xenos_results import get_wer as get_wer_x


# Parameters
P = {
    '–': {
        'lang': ('n-gram', 'none'),
        'get-wer': get_wer_d,
        'weight': None,
    },
    'n-gram lg': {
        'lang': ('n-gram', 'n-gram'),
        'get-wer': get_wer_d,
        'weight': None,
    },
    'rnn txt': {
        'lang': ('n-gram', 'rnn-xenos'),
        'get-wer': get_wer_x,
        'weight': 10,
    },
    'rnn img': {
        'lang': ('n-gram', 'rnn-xenos-img-g'),
        'get-wer': get_wer_x,
        'weight': 10,
    },
}

SIZES = [2 ** s for s in range(11, 17)]
# SIZES = [4096]

NOISE_TO_DATASET = {
    '∞': 'test_uav',
}
for noise in "20 10 5".split():
    NOISE_TO_DATASET[noise] = f"test_uav_noise_snr_{noise}"


def stats(xs):
    xs = list(xs)
    return mean(xs), sem(xs)


def load_data():
    wers = {
        (s, n, noise): stats(
            P[s]['get-wer'](
                *P[s]['lang'],
                nr_sents=n,
                fold=f + 1,
                dset=NOISE_TO_DATASET[noise],
                lmwt=P[s]['weight'])
            for f in range(5))
        for s, n, noise in product(P, SIZES, NOISE_TO_DATASET)
    }

    wers = pd.DataFrame(wers).T.reset_index()
    wers = wers.rename(columns={
        'level_0': 'rescoring',
        'level_1': 'size',
        'level_2': 'SNR',
        0: 'WER',
        1: 'SEM',
    })

    wers['WER-min'] = wers['WER'] - 2 * wers['SEM']
    wers['WER-max'] = wers['WER'] + 2 * wers['SEM']

    return wers


def main():
    wers = load_data()


if __name__ == '__main__':
    main()
