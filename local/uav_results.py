#!/usr/bin/python3

import argparse
import os
import pdb

from collections import OrderedDict

from glob import glob

from ted_results import ROOT
from ted_results import get_wers as get_wers_ted


BASEPATHS = {
    "ted": "exp",
    "uav": "exp_uav",
    "uav_grammar": "exp_uav_grammar",
}

SUFFIXES = {
    "ted": "_dev",
    "uav": "_test_uav",
    "uav_grammar": "_test_uav",
}

SNRS = [20, 10, 5, 0]

for snr in map(str, SNRS):

    BASEPATHS["uav_noise_" + snr] = "exp_uav"
    BASEPATHS["uav_grammar_noise_" + snr] = "exp_uav_grammar"

    SUFFIXES["uav_noise_" + snr] = "_test_uav_noise_snr_" + snr
    SUFFIXES["uav_grammar_noise_" + snr] = "_test_uav_noise_snr_" + snr


def get_stage_dir(stage, rescore, split):
    suffix_rescore = '_rescore' if rescore else ''
    return STAGE_DIRS[stage] + SUFFIXES[split] + suffix_rescore


def get_path_to_scores(stage, rescore, split):
    return os.path.join(ROOT, BASEPATHS[split], get_stage_dir(stage, rescore, split))


def get_wers(path):

    def get_hyper_params(path):
        *_, lmwt = path.split('_')
        return int(lmwt)

    def get_wer(path):
        with open(path, 'r') as f:
            for line in f.readlines():
                if 'WER' in line:
                    _, wer, *_ = line.split()
                    return float(wer)
        print("-- ERROR Could not parse path")
        print(path)
        raise ValueError

    g = os.path.join(path, 'wer_*')
    return {get_hyper_params(f): get_wer(f) for f in glob(g)}


def get_wer(stage, split, rescore):
    """Pick the value for the LMWT hyper-parameter based on TED-LIUM DEV."""
    wers_ted = get_wers_ted(get_path_to_scores(stage, rescore, 'ted'))
    wers_uav = get_wers(get_path_to_scores(stage, rescore, split))
    (i, _), _ = min(wers_ted.items(), key=lambda t: t[1])
    wer = wers_uav[i]
    return wer


STAGE_DIRS = OrderedDict([
    ('tri1-nosp', 'tri1/decode_nosp'),
    ('tri2-nosp', 'tri2/decode_nosp'),
    ('tri2', 'tri2/decode'),
    ('tri3', 'tri3/decode'),
    ('chain-tdnn', 'chain_cleaned/tdnn1f_sp_bi/decode'),
])


STAGES = list(STAGE_DIRS.keys())

ROW_FORMAT = '| {:10s} | {:5.2f} | {:5.2f} |'


def main():
    parser = argparse.ArgumentParser(description='Script to fetch WER results for the UAV set.')
    parser.add_argument('split', choices=BASEPATHS, help='which results to fetch')
    args = parser.parse_args()

    for stage in STAGES:
        wer1 = get_wer(stage, args.split, rescore=False)
        wer2 = get_wer(stage, args.split, rescore=True)
        print(ROW_FORMAT.format(stage, wer1, wer2))


if __name__ == '__main__':
    main()
