#!/bin/bash

set -e
set -x

snr=$1

if [[ $# -eq 0 ]] ; then
    echo 'Missing SNR value'
    exit 1
fi

# Useful paths
I=/home/doneata/data/uav-commands/audio_and_noise/snr_$snr  # Input
O=data/test_uav_noise_snr_$snr  # Output

ORIG=data/test_uav  # Original, clean dataset

mkdir -p $O

# wav.scp
find $I -name '*wav' | sort > /tmp/paths.txt
find $I -name '*wav' -printf "%f\n" | sort > /tmp/files.txt

paste -d" " \
    <(cut -f1 -d"." /tmp/files.txt) \
    <(cat /tmp/paths.txt) \
    > $O/wav.scp

# utt2spk spk2utt text
for f in utt2spk spk2utt text; do 
    cp $ORIG/$f $O/.
done
