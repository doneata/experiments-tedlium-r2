#!/usr/bin/python3
from itertools import product

from uav_results import (
    get_wer,
)

def main():
    STAGE = 'chain-tdnn'

    RESCORES = [False, True]
    SNRS = [None, 20, 10, 5, 0]
    SPLITS = ['uav', 'uav_grammar']

    for split, rescore in product(SPLITS, RESCORES):
        for snr in SNRS:
            if snr is not None:
                split1 = split + '_noise_' + str(snr)
            else:
                split1 = split
            wer = get_wer(STAGE, split1, rescore)
            print(wer, end=' | ')
        print()


if __name__ == '__main__':
    main()
