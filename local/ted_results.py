#!/usr/bin/python3

import argparse
import os
import pdb

from glob import glob

from subprocess import (
    call,
)


ROOT = "/home/doneata/work/experiments-tedlium-r2"
BASEPATH = "exp/chain_cleaned/tdnn1f_sp_bi"

CONFIG = {
    'tdnn.small-lm': {
        'directory': 'decode_{}',
        'decoding-lm': 'small-lm',
        'rescoring-lm': '',
    },
    'tdnn.large-lm': {
        'directory': 'decode_{}_rescore',
        'decoding-lm': 'small-lm',
        'rescoring-lm': 'large-lm',
    },
    # Old type of rescoring: prior to the rnnlm branch being merged into master
    'tdnn.rnnlm-alpha': {
        'directory': 'decode_{}.kaldirnnlm.lat.4gram',
        'decoding-lm': 'small-lm',
        'rescoring-lm': 'rnnlm-v1',
    },
    'tdnn.rnnlm-alpha.lattice': {
        'directory': 'decode_{}_rescore.kaldirnnlm.lattice.4gram',
        'decoding-lm': 'small-lm',
        'rescoring-lm': 'rnnlm-v1 (lattice)',
    },
    'tdnn.rnnlm-alpha.nbest': {
        'directory': 'decode_{}_rescore.kaldirnnlm.nbest.4gram',
        'decoding-lm': 'small-lm',
        'rescoring-lm': 'rnnlm-v1 (n-best)',
    },
    'tdnn.tfrnnlm.lattice': {
        'directory': 'decode_{}_rescore.tfrnnlm.lattice.4gram',
        'decoding-lm': 'small-lm',
        'rescoring-lm': 'rnnlm-v1 (n-best)',
    },
}


def get_wers(path):

    def get_hyper_params(path):
        filename = os.path.basename(path)
        _, lmwt, wip = filename.split('_')
        return int(lmwt), float(wip)

    def get_wer(folder):
        path = os.path.join(folder, 'ctm.filt.filt.sys')
        with open(path, 'r') as f:
            for line in f.readlines():
                if 'Sum/Avg' in line:
                    _, _, _, errors, _, _ = line.split('|')
                    _, _, _, _, wer, _ = errors.split()
                    return float(wer)

    g = os.path.join(path, 'score_*')
    return {get_hyper_params(f): get_wer(f) for f in glob(g)}


def main():
    parser = argparse.ArgumentParser(description='Script to obtain WER from the experiments.')
    parser.add_argument('method', choices=CONFIG, help='name of the method')
    args = parser.parse_args()

    d = CONFIG[args.method]['directory']
    p = os.path.join(ROOT, BASEPATH, d)
    wers_dev = get_wers(p.format('dev'))
    wers_test = get_wers(p.format('test'))

    hyper_params, wer_dev = min(wers_dev.items(), key=lambda t: t[1])
    wer_test = wers_test[hyper_params]

    decoding_lm = CONFIG[args.method]['decoding-lm']
    rescoring_lm = CONFIG[args.method]['rescoring-lm']

    STR_FMT = '| {:25s} | {:10s} | {:10s} | {:5.2f} | {:5.2f} | {:2d}, {:.1f} |'
    print(STR_FMT.format(args.method, decoding_lm, rescoring_lm, wer_dev, wer_test, *hyper_params))


if __name__ == '__main__':
    main()
