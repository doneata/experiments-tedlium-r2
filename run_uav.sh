#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

nj=8
decode_nj=8  # note: should not be greater than the number of speakers
             # We decode with 4 threads, so this will be too many jobs if
             # you're using run.pl.
stage=0
dset=test_uav

. utils/parse_options.sh # accept options

exp_dir=exp_uav
test_dir=data/${dset}

if [ $stage -eq 0 ]; then
    steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $test_dir
    steps/compute_cmvn_stats.sh $test_dir
fi

if [ $stage -eq 1 ]; then
    steps/decode.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri1/graph_nosp \
        $test_dir \
        $exp_dir/tri1/decode_nosp_${dset}

    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri1/graph_nosp \
        $exp_dir/tri1/decode_nosp_${dset}

    steps/lmrescore_const_arpa.sh \
        --cmd "$decode_cmd" \
        --skip_scoring true \
        data/lang_nosp \
        data/lang_nosp_rescore \
        $test_dir \
        $exp_dir/tri1/decode_nosp_${dset} \
        $exp_dir/tri1/decode_nosp_${dset}_rescore

    local/score_basic.sh \
        $test_dir \
        data/lang_nosp_rescore \
        $exp_dir/tri1/decode_nosp_${dset}_rescore
fi

if [ $stage -eq 2 ]; then
    # Decode
    steps/decode.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri2/graph_nosp \
        $test_dir \
        $exp_dir/tri2/decode_nosp_${dset}

    # Evaluate
    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri2/graph_nosp \
        $exp_dir/tri2/decode_nosp_${dset}

    # Rescore
    steps/lmrescore_const_arpa.sh \
        --cmd "$decode_cmd" \
        --skip_scoring true \
        data/lang_nosp \
        data/lang_nosp_rescore \
        $test_dir \
        $exp_dir/tri2/decode_nosp_${dset} \
        $exp_dir/tri2/decode_nosp_${dset}_rescore

    # Evaluate
    local/score_basic.sh \
        $test_dir \
        data/lang_nosp_rescore \
        $exp_dir/tri2/decode_nosp_${dset}_rescore
fi

if [ $stage -eq 3 ]; then
    steps/decode.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri2/graph \
        $test_dir \
        $exp_dir/tri2/decode_${dset}

    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri2/graph \
        $exp_dir/tri2/decode_${dset}

    steps/lmrescore_const_arpa.sh \
        --cmd "$decode_cmd" \
        --skip_scoring true \
        data/lang \
        data/lang_rescore \
        $test_dir \
        $exp_dir/tri2/decode_${dset} \
        $exp_dir/tri2/decode_${dset}_rescore

    local/score_basic.sh \
        $test_dir \
        data/lang_rescore \
        $exp_dir/tri2/decode_${dset}_rescore
fi

if [ $stage -eq 4 ]; then
    steps/decode_fmllr.sh \
        --cmd "$decode_cmd"  \
        --nj $decode_nj \
        --num-threads 4 \
        --skip_scoring true \
        $exp_dir/tri3/graph \
        $test_dir \
        $exp_dir/tri3/decode_${dset}

    local/score_basic.sh \
        $test_dir \
        $exp_dir/tri3/graph \
        $exp_dir/tri3/decode_${dset}

    steps/lmrescore_const_arpa.sh \
        --cmd "$decode_cmd" \
        --skip_scoring true \
        data/lang \
        data/lang_rescore \
        $test_dir \
        $exp_dir/tri3/decode_${dset} \
        $exp_dir/tri3/decode_${dset}_rescore

    local/score_basic.sh \
        $test_dir \
        data/lang_rescore \
        $exp_dir/tri3/decode_${dset}_rescore
fi

if [ $stage -eq 5 ]; then
    nnet3_affix=_cleaned
    tdnn_affix=1f
    d=${exp_dir}/chain${nnet3_affix}/tdnn${tdnn_affix}_sp_bi

    # Extract iVectors
    utils/copy_data_dir.sh data/$dset data/${dset}_hires

    steps/make_mfcc.sh \
        --nj $nj \
        --mfcc-config conf/mfcc_hires.conf \
        --cmd "$train_cmd" \
        data/${dset}_hires

    steps/compute_cmvn_stats.sh \
        data/${dset}_hires

    utils/fix_data_dir.sh \
        data/${dset}_hires

    steps/online/nnet2/extract_ivectors_online.sh \
        --cmd "$train_cmd" \
        --nj "$nj" \
        data/${dset}_hires \
        ${exp_dir}/nnet3${nnet3_affix}/extractor \
        ${exp_dir}/nnet3${nnet3_affix}/ivectors_${dset}_hires

    # Decode
    steps/nnet3/decode.sh \
        --num-threads 4 \
        --nj $decode_nj \
        --cmd "$decode_cmd" \
        --acwt 1.0 \
        --post-decode-acwt 10.0 \
        --online-ivector-dir ${exp_dir}/nnet3${nnet3_affix}/ivectors_${dset}_hires \
        --skip_scoring true \
        $d/graph \
        data/${dset}_hires \
        $d/decode_${dset}

    # Evaluate
    local/score_basic.sh \
        data/${dset}_hires \
        $d/graph \
        $d/decode_${dset}

    # Rescore
    steps/lmrescore_const_arpa.sh \
        --cmd "$decode_cmd" \
        --skip_scoring true \
        data/lang \
        data/lang_rescore \
        data/${dset}_hires \
        $d/decode_${dset} \
        $d/decode_${dset}_rescore

    # Evaluate
    local/score_basic.sh \
        data/${dset}_hires \
        data/lang_rescore \
        $d/decode_${dset}_rescore
fi
